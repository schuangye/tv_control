package com.scy.simkey;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

public class BootRestartReceiver extends BroadcastReceiver {

	private static final String TAG = "BootRestartReceiver";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		
		// TODO Auto-generated method stub
		  if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED))
		  {
			  Log.i(TAG, "自动启动开始..."); 
			  Intent service = new Intent(context, com.scy.simkey.http.httpserver.SystemHttpService.class);
			  service.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
			  context.startService(service);
			 
			   //记录日志
			  Log.i(TAG, "自动服务自动启动结束"); 
		  }
	}
}
