package com.scy.simkey.common;

import java.util.ArrayList;
import java.util.List;


public class Device {
	private String         mDeviceID;
	private List<String>   mDeviceAddrList;
	
	public String getmDeviceID() {
		return mDeviceID;
	}
	public void setmDeviceID(String mDeviceID) {
		this.mDeviceID = mDeviceID;
	}
	public List<String> getmDeviceAddrList() {
		return mDeviceAddrList;
	}
	public void setmDeviceAddrList(List<String> mDeviceAddrList) {
		this.mDeviceAddrList = new ArrayList<String>(mDeviceAddrList.size());  
    	for(String uuid : mDeviceAddrList){
    		this.mDeviceAddrList.add(uuid);  
    	}
	}
}
