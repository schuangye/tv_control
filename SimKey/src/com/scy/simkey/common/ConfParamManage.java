package com.scy.simkey.common;

import java.util.UUID;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class ConfParamManage {

	private static final String TAG = "ConfigParamManage";
	private static final String SIM_KEY_PARAM = "SimKeyParam";
	
	//社保id
	public static String  mDeviceID = "";
	
	//监听端口
	public  static int mListenPort = 8090; 
	
	//加载参数
	public static void loadParam(Context context)
	{
		//获取存储
		SharedPreferences preferences = getSharedPreferences(context);
		
		//获取设备id
		mDeviceID = preferences.getString(ConfParamName.mDeviceID, "");
		
		//获取端口号
		mListenPort = preferences.getInt(ConfParamName.mListenPort, 8090);
		
		if(mDeviceID == ""){
			mDeviceID = generateDeviceID();
			
			//保存设备id
			saveParam(context);
		}
	}
	
	//保存参数
	public static int saveParam(Context context)
	{
		//获取编辑器
    	Editor edit = getEditor(context);
 
    	//保存设备id
         edit.putString(ConfParamName.mDeviceID, mDeviceID);
    	
    	//保存端口号
    	edit.putInt(ConfParamName.mListenPort, mListenPort);
    	
    	//提交数据
    	if(edit.commit()){
    		Log.i(TAG, "commit param fail"); 
    		return -1;
    	}
    	
		return 0;
	}
	
	//生成设备id
	private static String generateDeviceID(){
		
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
	
	//获取editor
	private static Editor getEditor(Context context) 
	{
		SharedPreferences preferences = getSharedPreferences(context);
		return preferences.edit();
	}
	
	//获取SharedPreferences
	private static SharedPreferences getSharedPreferences(Context context)
	{
		SharedPreferences preferences = context.getSharedPreferences(SIM_KEY_PARAM, 
				                                                     Context.MODE_PRIVATE);
		return preferences;
	}
	
}
