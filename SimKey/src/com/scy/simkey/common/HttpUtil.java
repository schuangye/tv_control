package com.scy.simkey.common;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;

public class HttpUtil{

	//发送键值
	public static final String CMD_CONNECT_DEVICE  = "ConnectDevice";
	
	//发送键值
	public static final String CMD_SIM_KEY  = "SimKey";
	
	//取httphandler名利
	public static String getHttpHandlerCmd(String cmd){
		return "/" + cmd;
	}
	
	//从请求中取body
	public static String getBodyFromRequest(HttpRequest request) throws IllegalStateException, IOException{
		HttpEntity req = ((BasicHttpEntityEnclosingRequest)request).getEntity();
		int length = (int)req.getContentLength();                
        InputStream inputStream=req.getContent();
        byte array[] = new byte[length];
        inputStream.read(array);
        String body = new String(array,"UTF-8");
        body = URLDecoder.decode(body,"UTF-8");
        return body;
	}
	
	//从相应中去body
	public static String getBodyFromResponse(HttpResponse response) throws IllegalStateException, IOException{
		HttpEntity rsp = response.getEntity();
		int length = (int)rsp.getContentLength();                
        InputStream inputStream = rsp.getContent();
        byte array[] = new byte[length];
        inputStream.read(array);
        String body = new String(array,"UTF-8");
        body = URLDecoder.decode(body,"UTF-8");
        return body;
	}
}
