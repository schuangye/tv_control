package com.scy.simkey.common;

import java.io.OutputStream;

import android.content.Context;
import android.content.Intent;

public class RootShellCmd {
	
    /**
     * 模拟类型枚举
     * 
     * @author scy
     *
     */
    public enum simTypeEnum {
        EXEC_ROOT,                 //用root执行
        EXEC_NO_ROOT, 
        EXEC_BROADCASE;
    }
    
	private static RootShellCmd instance = null;  
    private OutputStream mOS = null;  
    public Context mctx = null;
    
    public static synchronized RootShellCmd getInstance() {
       //使用时生成实例，提高了效率！  
    	if (instance==null){
    		instance=new RootShellCmd(); 
    	}
    		 
    	return instance;  
	 } 
    
    public synchronized void Init(Context ctx) {
    		if(mctx == null && ctx != null){
    			mctx = ctx;
    		}
 	 } 
    /** 
     * 执行shell指令 
     *  
     * @param cmd 
     *            指令 
     */  
    public synchronized void exec(String cmd) {  
        try {  
            if (mOS == null) {  
            	mOS = Runtime.getRuntime().exec("su").getOutputStream();  
            }  
            mOS.write(cmd.getBytes());  
            mOS.flush(); 
            
        } catch (Exception e) {  
            e.printStackTrace(); 
        }  
    }  
    /** 
     * 执行shell指令 
     *  
     * @param cmd 
     *            指令 
     */  
    public void execNoSu(String cmd) {  
        try {  
        	 Runtime.getRuntime().exec(cmd);
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
    } 
    /** 
     * 发送按键广播
     *  
     * @param cmd 
     *            指令 
     */  
    public void sendKeyCodeBroadcast(String cmd) {  
        if(mctx != null){
            Intent intent = new Intent();  
            intent.setAction("com.scy.simkey.KEYCODE");  
            intent.putExtra("keycode", cmd); 
            mctx.sendBroadcast(intent);
        }
    } 
    /** 
     * 后台模拟全局按键 
     *  
     * @param string 
     *            键值 
     */  
    public synchronized void simulateKey(String string, simTypeEnum type) {
    	String cmd = "input keyevent " + string + "\n";
    	switch(type){
    	case EXEC_ROOT:
    		exec(cmd);
    		break;
    	case EXEC_NO_ROOT:	
    		execNoSu(cmd);
    		break;
    	case EXEC_BROADCASE:
    		sendKeyCodeBroadcast(string);
    		break;
    	default:
    	}
    }
    /** 
     * 后台模拟全局按键 
     *  
     * @param string 
     *            键值 
     */  
    public synchronized void simulateKey(String keycode) {  
    	exec("input keyevent " + keycode + "\n");
    	if(mOS == null){
    		sendKeyCodeBroadcast(keycode);
    	}
    }
}
