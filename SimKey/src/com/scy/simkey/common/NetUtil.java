package com.scy.simkey.common;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import org.apache.http.conn.util.InetAddressUtils;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.MulticastLock;
import android.util.Log;

public class NetUtil {

	private static final String TAG = "MulticastNetUtil";
	private static MulticastLock mMulticastLock;
	
	//开启组播
	public static void enableMulticast(Context context){
		 WifiManager wifiManager=(WifiManager)context.getSystemService(Context.WIFI_SERVICE);
		 if(wifiManager != null){
			 mMulticastLock = wifiManager.createMulticastLock("MulticastNetUtil.ts"); 
			 if(mMulticastLock != null){
				 mMulticastLock.acquire(); 
			 }	 
		 }
	}
	
	//禁用组播
	public static void disenableMulticast(){
		 if(mMulticastLock != null){
			 mMulticastLock.release(); 
		 }
	}
	
	//获取本机地址
	public static String getLocalHostIp(){
		String ipaddress = "";
		Enumeration<NetworkInterface> en;
		try {
			en = NetworkInterface.getNetworkInterfaces();
			//遍历所用的网络接口
			while (en.hasMoreElements()){
				
				// 得到每一个网络接口绑定的所有ip
				NetworkInterface nif = en.nextElement();
				Enumeration<InetAddress> inet = nif.getInetAddresses();
				while (inet.hasMoreElements()){
					InetAddress ip = inet.nextElement();
			        if (!ip.isLoopbackAddress() && InetAddressUtils.isIPv4Address(ip.getHostAddress()))
	                {
			        	if(ipaddress == ""){
			        		ipaddress = ip.getHostAddress();
			        	}else{
			        		ipaddress += ";" + ip.getHostAddress();
			        	}
	                }
				}
			}
			
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			Log.i(TAG, "en = NetworkInterface.getNetworkInterfaces() fail. "+ e.getMessage()); 
		}
		
		return ipaddress;
	}
	
}





