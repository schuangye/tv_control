package com.scy.simkey;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.scy.simkey.common.ConfParamManage;
import com.scy.simkey.common.Device;
import com.scy.simkey.common.JsonUtil;
import com.scy.simkey.common.NetUtil;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private int QR_WIDTH = 200, QR_HEIGHT = 200;
	private Device mdevice = new Device();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//启动扫描服务
		StartService(com.scy.simkey.http.httpserver.SystemHttpService.class);
		
		setContentView(R.layout.activity_main);
		
		//显示信息
		showInfo();
		
		//获取保存
		Button butsave = (Button)findViewById(R.id.butSave);
		butsave.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				EditText txtport = (EditText)findViewById(R.id.editPort);
				if(txtport.getText() != null){
					int port = Integer.parseInt(txtport.getText().toString());
					if(port > 0){
						ConfParamManage.mListenPort = port;
						ConfParamManage.saveParam(getApplicationContext());
						Toast.makeText(getApplicationContext(), "保存端口成功", Toast.LENGTH_LONG).show();
					}
				}
			}
		});
		
		//获取保存
		Button butrefresh = (Button)findViewById(R.id.butRefresh);
		butrefresh.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				showInfo();
			}
		});
		
		//获取保存
		Button btstart = (Button)findViewById(R.id.butStart);
		btstart.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				StartService(com.scy.simkey.http.httpserver.SystemHttpService.class);
				Toast.makeText(getApplicationContext(), "SimKey服务启动", Toast.LENGTH_LONG).show();
			}
		});
		
		//获取保存
		Button btstop = (Button)findViewById(R.id.butStop);
		btstop.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {
				StopService(com.scy.simkey.http.httpserver.SystemHttpService.class);
				Toast.makeText(getApplicationContext(), "SimKey服务停止", Toast.LENGTH_LONG).show();
			}
		});
	}
	
	private void StartService(Class service){
		Intent intent = new Intent(this, service); 
		this.startService(intent);
	}
	
	private void StopService(Class service){
		Intent intent = new Intent(this, service); 
		this.stopService(intent);
	}
	
	//显示信息
	private void showInfo(){
		
		//显示ip
		TextView txtip= (TextView)findViewById(R.id.textLocalIP);
		if(txtip != null){
			String ip = NetUtil.getLocalHostIp();
			txtip.setText(ip);
		}
		
		//显示设备id
		TextView txtdevid = (TextView)findViewById(R.id.TextDeviceID);
		if(txtdevid != null){
			txtdevid.setText(ConfParamManage.mDeviceID);
			mdevice.setmDeviceID(ConfParamManage.mDeviceID);
		}
		
		//显示url
		List<String> tmpaddlist = new ArrayList<String>();
		TextView txturl = (TextView)findViewById(R.id.TextUrl);
		if(txturl != null){
			String ipstr = NetUtil.getLocalHostIp();
			String[] strarray = ipstr.split(";"); 
			String url = "";
			for (int i = 0; i < strarray.length; i++){
				String tmpurl = "http://" + strarray[i] + ":" + ConfParamManage.mListenPort + "/SimKey";
				if(url == ""){
					url = tmpurl;
				}else{
					url += "\n" + tmpurl;
				}
				
				tmpaddlist.add(strarray[i] + ":" + ConfParamManage.mListenPort);
			}
			txturl.setText(url);
		}
		
		mdevice.setmDeviceAddrList(tmpaddlist);
		
		//显示监听端口
		EditText txtport = (EditText)findViewById(R.id.editPort);
		if(txtport != null){
			txtport.setText(Integer.toString(ConfParamManage.mListenPort));
		}
		
		//将对象转换成json串
		String jsonstr = JsonUtil.object2Json(mdevice);
		createQRImage(jsonstr);
	}
	
	//生成二维码
	public void createQRImage(String url)
	{
		try
		{
			//判断URL合法性
			if (url == null || url.equals("") || url.length() < 1)
			{
				return;
			}
			
			Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
			hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
			
			//图像数据转换，使用了矩阵转换
			BitMatrix bitMatrix = new QRCodeWriter().encode(url, BarcodeFormat.QR_CODE, QR_WIDTH, QR_HEIGHT, hints);
			int[] pixels = new int[QR_WIDTH * QR_HEIGHT];
			
			//下面这里按照二维码的算法，逐个生成二维码的图片，两个for循环是图片横列扫描的结果
			for (int y = 0; y < QR_HEIGHT; y++)
			{
				for (int x = 0; x < QR_WIDTH; x++)
				{
					if (bitMatrix.get(x, y))
					{
						pixels[y * QR_WIDTH + x] = 0xff000000;
					}
					else
					{
						pixels[y * QR_WIDTH + x] = 0xffffffff;
					}
				}
			}
			//生成二维码图片的格式，使用ARGB_8888
			Bitmap bitmap = Bitmap.createBitmap(QR_WIDTH, QR_HEIGHT, Bitmap.Config.ARGB_8888);
			bitmap.setPixels(pixels, 0, QR_WIDTH, 0, 0, QR_WIDTH, QR_HEIGHT);
			
			//显示到一个ImageView上面
			ImageView imageview = (ImageView)findViewById(R.id.imageView1);
			imageview.setImageBitmap(bitmap);
		}
		catch (WriterException e)
		{
			e.printStackTrace();
		}
	}
}















