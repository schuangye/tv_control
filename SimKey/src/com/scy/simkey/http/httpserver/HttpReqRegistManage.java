package com.scy.simkey.http.httpserver;

import org.apache.http.protocol.HttpRequestHandlerRegistry;

import com.scy.simkey.common.HttpUtil;
import com.scy.simkey.http.httphandler.ConnectCmdHandler;
import com.scy.simkey.http.httphandler.SimKeyCmdHandler;



public class HttpReqRegistManage {
	
	private static final String ALL_PATTERN = "*";
	private static HttpRequestHandlerRegistry mHttpReqRegist = null;
	
	public static HttpRequestHandlerRegistry getReqRegistHanler(){
		
		if(mHttpReqRegist == null){
			mHttpReqRegist = new HttpRequestHandlerRegistry();
			
			//ע��handle
			RegisterCmdHandle();
		}
		
		
		return mHttpReqRegist;
	}
	
	//ע��handle
	private static void RegisterCmdHandle(){
		mHttpReqRegist.register(ALL_PATTERN, new HomeCmdHandler());
		mHttpReqRegist.register(HttpUtil.getHttpHandlerCmd(HttpUtil.CMD_CONNECT_DEVICE), new ConnectCmdHandler());
		mHttpReqRegist.register(HttpUtil.getHttpHandlerCmd(HttpUtil.CMD_SIM_KEY), new SimKeyCmdHandler());
	}
}
