package com.scy.simkey.http.httpmessage;

public class HttpMessageReq{
	private String   mDeviceID;

	public HttpMessageReq(){
		mDeviceID = "";
	}

	public String getmDeviceID() {
		return mDeviceID;
	}

	public void setmDeviceID(String mDeviceID) {
		this.mDeviceID = mDeviceID;
	}
}
