# TV_Control

#### 介绍
TV_Control是一款可以通过手机控制Android机顶盒的程序，包括了Android机顶盒上的程序，和手机H5的程序


#### 使用教程

1.  浏览手机端，下载机顶盒端APP，机顶盒端安装APP
2.  打开机顶盒端APP，生成bangding二维码
3.  手机端通过输入法扫描二维码，或者手工输入绑定号
4.  完成设备绑定，就可以模拟遥控器控制机顶盒

#### 软件截图

![输入图片说明](https://images.gitee.com/uploads/images/2021/0123/174927_b458e417_305663.jpeg "手机截图.jpg")

#### 附录

手机端预览地址：http://47.106.69.236/tv_control/tvcontrol.htm

![输入图片说明](https://images.gitee.com/uploads/images/2021/0125/111405_25f302a2_305663.png "屏幕截图.png")


#### 技术支持

![输入图片说明](https://images.gitee.com/uploads/images/2021/0125/152633_a02d3c60_305663.png "屏幕截图.png")